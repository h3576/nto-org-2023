m = int(input())
n = int(input())

metal = []

for i in range(m):
    p, w = map(int, input().split())
    metal.append((p, w, p/w))

metal.sort(key = lambda  x: x[2])

total_w = 0
total_p = 0
for l in metal:
    if l[1] + total_w <= n:
        total_w += l[1]
        total_p += l[0]

print(total_p)