n = int(input())
word = input()

polyndroms = 0

for l in range(n, len(word) + 1):
    for i in range(len(word) - l + 1):
        if word[i:i+l] == word[i:i+l][::-1]:
            polyndroms += 1

print(polyndroms)