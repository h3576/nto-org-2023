m = int(input())
ans = 1
for i in range(m):
    info = list(map(float, input().split()))
    n = int(info[0])
    mode = int(info[1])
    pre_ans = 1
    if mode == 0:
        for j in range(2, len(info)):
            pre_ans *= info[j]

    elif mode == 1:
        for j in range(2, len(info)):
            pre_ans *= (1 - info[j])
        pre_ans = 1 - pre_ans
    ans *= pre_ans

print(round(ans, 3))