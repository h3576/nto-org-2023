import math

import numpy as np

n = int(input())

used_nums = set()

matrix = []
for i in range(n):
    data = list(map(int, input().split()))
    for d in data:
        used_nums.add(d)
    matrix.append(data)

matrix = np.array(matrix)

triangles = 0

for i in used_nums:
    coords = np.where(matrix == i)
    for j in range(len(coords[0])):
        for k in range(j + 1, len(coords[0])):
            for h in range(k + 1, len(coords[0])):
                side_a = ((coords[0][j] - coords[0][k]) ** 2 + (coords[1][j] - coords[1][k]) ** 2) ** 0.5
                side_b = ((coords[0][k] - coords[0][h]) ** 2 + (coords[1][k] - coords[1][h]) ** 2) ** 0.5
                side_c = ((coords[0][h] - coords[0][j]) ** 2 + (coords[1][h] - coords[1][j]) ** 2) ** 0.5
                if side_a == side_b or side_b == side_c or side_c == side_a:
                    if (side_a + side_b) != side_c and (side_b + side_c) != side_a and (side_c + side_a) != side_b:
                        triangles += 1

print(triangles)