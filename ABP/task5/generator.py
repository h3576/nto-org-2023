N = 20
ribs = []
for i in range(1, N + 1):
    for j in range(i, N + 1):
        ribs.append(f"{i} {j}")

f = open("task.txt", "w")

f.write(str(N) + " " + str(len(ribs)) + "\n")
for rib in ribs:
    f.write(rib + "\n")

f.close()