import sys

sys.setrecursionlimit(40000)

ans = []


def find_euler_path(point):
    for p in graph[point]:
        graph[point].remove(p)
        graph[p].remove(point)
        find_euler_path(p)
    ans.append(point)


n, m = map(int, input().split())

graph = [[] for i in range(n + 1)]

ex_set = set()

for i in range(m):
    a, b = map(int, input().split())
    graph[a].append(b)
    graph[b].append(a)
    ex_set.add((min(a, b), max(a, b)))

to_add = []
for i in range(1, n + 1):
    if len(graph[i]) % 2 != 0:
        to_add.append(i)

if len(to_add) == 1 or len(to_add) > 2:
    print("IMPOSSIBLE")
    exit()

if len(to_add) == 2:
    find_euler_path(to_add[0])
else:
    find_euler_path(1)

for i in range(len(ans)):
    print(ans[i], end=" ")
