n, m, x = map(int, input().split())
degustations = []
for i in range(m):
    degustations.append(list(map(int, input().split())))

avg_deg = {}


for i in range(n):
    sm = 0
    for j in range(m):
        sm += degustations[j][i]
    avg_deg[i] = sm / m


avg_deg = sorted(avg_deg.items(), key=lambda x: x[1], reverse=True)

candys_to_use = []
candy_sum = 0

i = 0
while candy_sum < x and i < len(avg_deg):
    candy_sum += avg_deg[i][1]
    candys_to_use.append(avg_deg[i][0])
    i += 1

candys_to_use = sorted(candys_to_use)

if x > candy_sum:
    print("IMPOSSIBLE")
else:
    print(len(candys_to_use))
    for j in range(len(candys_to_use)):
        print(candys_to_use[j] + 1, end=" ")