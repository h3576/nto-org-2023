n = int(input())
toys = list(map(int, input().split()))

storage = set()
max_storage = 0
for toy in toys:
    if toy not in storage:
        storage.add(toy)
    else:
        storage.remove(toy)
    max_storage = max(len(storage), max_storage)

print(max_storage)