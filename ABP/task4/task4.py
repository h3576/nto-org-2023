def get_time(elem):
    if len(links[elem]) == 0:
        return times[elem]
    else:
        local_times = [get_time(detail) for detail in links[elem]]
        return max(local_times) + times[elem]


n = int(input())

nums = set()
for i in range(1, n + 1):
    nums.add(i)

links = [[] for i in range(n + 1)]
times = [0 for i in range(n + 1)]

for i in range(n):
    data = list(map(int, input().split()))
    times[i + 1] = data[0]
    if data[1] > 0:
        for j in range(2, len(data)):
            links[i + 1].append(data[j])
            nums.remove(data[j])

root_elem = nums.pop()

print(get_time(root_elem))
