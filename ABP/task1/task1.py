x, y, z = map(int, input().split())
print(x * z // y + min(x * z % y, 1))