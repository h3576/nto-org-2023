import math

t = int(input())

for i in range(t):
    min_area = 9999999999999999
    max_area = 0
    n = int(input())
    for j in range(1, int(math.sqrt(n))):
        if (n - j) % (1 + 2 * j) == 0:
            k = (n - j) // (1 + 2 * j)
            area = k * j
            min_area = min(area, min_area)
            max_area = max(area, max_area)

    if max_area == 0:
        print(-1)
    else:
        print(min_area, max_area)
