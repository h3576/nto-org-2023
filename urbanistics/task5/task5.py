n, m = map(int, input().split())

data = list(map(int, input().split()))

model = {}

for i, d in enumerate(data):
    if d not in model:
        model[d] = [0, 0]
    model[d][0] += 1
    model[d][1] = i

laps = 0

for l in model:
    laps = max(laps, model[l][0])



min_winner = 10**10
winner = 0

for l in model:
    if model[l][0] == laps:
        if model[l][1] < min_winner:
            min_winner = model[l][1]
            winner = l

print(winner)