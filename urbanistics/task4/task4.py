n, m = map(int, input().split())

people = []

row = []

for i in range(n):
    people.append(list(map(int, input().split())))
    for p in people[i]:
        row.append((p, i))
row.sort()

in_use = [0 for i in range(n)]
races = 0

left_min = 0
right_min = 0
delta_min = row[-1][0] - row[0][0]

b = 0
e = 0

races = 1
in_use[row[b][1]] += 1


while b < len(row) and e < len(row):
    if races < n:
        e += 1
        if e >= len(row):
            break
        in_use[row[e][1]] += 1
        if in_use[row[e][1]] == 1:
            races += 1
    else:
        if row[e][0] - row[b][0] < delta_min:
            delta_min = row[e][0] - row[b][0]
            left_min = b
            right_min = e
        in_use[row[b][1]] -= 1
        if in_use[row[b][1]] == 0:
            races -= 1
        b += 1

r = set()
for i in range(left_min, right_min + 1):
    if row[i][1] not in r:
        r.add(row[i][1])
        print(row[i][0], end=" ")