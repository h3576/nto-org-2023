from math import ceil, floor

x = int(input())

for i in range(x):
    m, n = map(int, input().split())
    remote = ceil((m + 1) / 2)
    for j in range(1, m):
        if (remote - 1) // j + (remote - 1) % j <= n:
            print(j)
            break

