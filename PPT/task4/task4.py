shortest_length = 999999999
shortest_way = []


def dfs(visited, length, point):
    # print(visited)
    global shortest_way
    global shortest_length
    if point in visited:
        return
    visited.append(point)
    if length < shortest_length and point == 5:
        shortest_length = length
        shortest_way = visited
    if point == 5:
        return
    for p in graph[point]:
        dfs(visited.copy(), length + graph[point][p], p)


graph = [
    {1: 4, 4: 2},
    {0: 4, 2: 5},
    {1: 5, 3: 2},
    {2: 2, 4: 3},
    {3: 3, 0: 2},
    {}
]

names = ["x0", "x1", "x2", "x3", "x4", "z"]

v1, v2, v3, v4 = map(int, input().split())

graph[5][1] = v1
graph[5][0] = v2
graph[5][3] = v3
graph[5][2] = v4

graph[1][5] = v1
graph[0][5] = v2
graph[3][5] = v3
graph[2][5] = v4

visited = []

dfs(visited, 0, 0)

for v in shortest_way:
    print(names[v], end=" ")