sizes = [1, 2, 4, 8, 16, 32, 64]

n = int(input())

p = len(sizes) - 1

while n > 0:
    if sizes[p] <= n:
        print(sizes[p], end=" ")
        n -= sizes[p]
    else:
        p -= 1