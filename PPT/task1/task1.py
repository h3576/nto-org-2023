k = int(input())

n = k // 5

if k % 5 > 0:
    n += 1

n -= 1

if n < 1:
    print("Экспресс")
elif n < 3:
    print("Экономный")
elif n < 5:
    print("Оптимальный")
elif n < 10:
    print("Долгий")
elif n < 15:
    print("Очень долгий")
else:
    print("Не рентабельный")