n, m = map(int,input().split())
h = [[0] + list(map(int,input().split())) + [0] for _ in range(n)]
h = [[0 for i in range(m + 2)]] + h + [[0 for j in range(m + 2)]]

windows = 0

for i in range(1, n + 1):
    for j in range(1, m + 1):
        block = h[i][j]
        if block > h[i - 1][j]:
            windows += block - h[i - 1][j]
        if block > h[i + 1][j]:
            windows += block - h[i + 1][j]
        if block > h[i][j - 1]:
            windows += block - h[i][j - 1]
        if block > h[i][j + 1]:
            windows += block - h[i][j + 1]

print(windows)