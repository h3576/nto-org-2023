r, s = map(int,input().split())
n = int(input())
p = [int(x) for x in input().split()]

pos = r
cross = 1
trees = 0
while pos < p[-1]:
    if p[cross] - r < pos:
        pos = p[cross] + r
        cross += 1
        continue
    trees += 2
    pos += s

print(trees)