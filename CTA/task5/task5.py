input()
elems = list(map(int, input().split()))
for el in elems:
    # print(elems)
    divs = 0
    cel = el
    while cel % 2 == 0:
        divs += 1
        cel //= 2

    if divs % 2 == 1:
        el //= 2

    d = 1
    answer = 0
    while d <= el:
        answer += (el - d) // (2 * d) + 1
        d *= 4
    print(int(answer))