w, h = map(int, input().split())
p, q = map(int, input().split())

area = 0
area += p * q
area += min(p, w - p) * q
area += min(q, h - q) * p
area += min(p, w - p) * min(q, h - q)
print(area)