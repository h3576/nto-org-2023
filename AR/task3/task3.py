w, h = map(int, input().split())

hall = [[0 for i in range(w)] for j in range(h)]

amounts = {}

for i in range(h):
    for j in range(w):
        if i + j not in amounts:
            amounts[i + j] = 0
        amounts[i + j] += 1

myKeys = list(amounts.keys())
myKeys.sort()
sorted_amounts = {i: amounts[i] for i in myKeys}

to_use = {}

l = 1
for num in sorted_amounts:
    to_use[num] = []
    while sorted_amounts[num] > 0:
        to_use[num].append(l)
        l += 1
        sorted_amounts[num] -= 1

for i in range(h):
    for j in range(w):
        hall[i][j] = to_use[i + j][-1]
        to_use[i + j].pop(-1)

for i in range(h):
    for j in range(w):
        print(hall[i][j], end=" ")
    print()