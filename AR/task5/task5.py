n, a, b = map(float, input().split())

prev = n
for i in range(1000000): # ограничиваем количество итераций
    next = a * prev - b * prev ** 2
    if abs(next - prev) < 1e-9: # достигнут предел
        print("{:.3f}".format(next))
        break
    elif i == 999999: # не достигнут предел
        print("-1.000")
    prev = next