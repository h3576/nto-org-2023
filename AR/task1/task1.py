n, w = map(int, input().split())

lines = 1
#line = ""
s = 0

for i in range(n):
    words = input().split()
    for word in words:
        if s == 0:
            s = len(word)
            #line += word
            continue
        if s + len(word) + 1 <= w:
            s += len(word) + 1
            #line += "_" + word
        else:
            lines += 1
            s = len(word)
            #line += "\n" + word

    if s != 0:
        #line += "\n"
        lines += 1
        s = 0
#print(line)
print(lines - 1)