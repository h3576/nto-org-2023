w, h = map(int, input().split())

hall = []

for i in range(h):
    hall.append(list(map(int, input().split())))

amounts = {}

for i in range(h):
    for j in range(w):
        if i + j not in amounts:
            amounts[i + j] = 0
        amounts[i + j] += 1

myKeys = list(amounts.keys())
myKeys.sort()
sorted_amounts = {i: amounts[i] for i in myKeys}

to_use = {}

l = 1
for num in sorted_amounts:
    to_use[num] = []
    while sorted_amounts[num] > 0:
        to_use[num].append(l)
        l += 1
        sorted_amounts[num] -= 1
OK = True
for i in range(h):
    for j in range(w):
        if hall[i][j] in to_use[i + j]:
            to_use[i + j].remove(hall[i][j])
        else:
            OK = False

if OK:
    print(1)
else:
    print(0)