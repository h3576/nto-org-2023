a, b, c = map(int, input().split())

n = int(input())

ea, eb, ec = 0, 0, 0

for i in range(n):
    pa, pb, pc = 0, 0, 0
    ai, bi, ci = map(int, input().split())
    if ai <= a:
        pa = 1
    if bi <= b:
        pb = 1
    if ci <= c:
        pc = 1
    if pa + pb + pc == 2:
        if pa == 0:
            ea += 1
        elif pb == 0:
            eb += 1
        else:
            ec += 1


if ea >= eb and ea >= ec:
    print("Algebra")
elif eb >= ec and eb >= ea:
    print("Geometry")
else:
    print("Physics")