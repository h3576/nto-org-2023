n = int(input())
students = list(map(int, input().split()))

state = 0
max_state = 0

for i in range(n):
    if students[i] == 1:
        state -= 1
    else:
        if state < 0:
            state = 1
        else:
            state += 1
    max_state = max(state, max_state)


print(sum(students) + max_state)