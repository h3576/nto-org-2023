n = int(input())
tasks = list(map(int, input().split()))

variants = [[0, 0, 0] for i in range(n)]

variants[0][0] = 0
variants[0][1] = tasks[0]
variants[0][2] = tasks[0]

for i in range(1, n):
    variants[i][0] = max([variants[i - 1][0], variants[i - 1][1], variants[i - 1][2]])
    variants[i][1] = max(variants[i - 1][0], variants[i - 1][2]) + tasks[i]
    variants[i][2] = variants[i - 1][0] + tasks[i]

print(max([variants[n - 1][0], variants[n - 1][1], variants[n - 1][2]]))