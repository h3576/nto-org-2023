#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
    int N;
    cin >>N;
    vector<int> slices;

    for (int i = 0; i < N; i++){
        int a;
        cin >>a;
        slices.push_back(a);
    }

    int area = -1;
    vector<int> indexes;

    for (int i = 0; i < N; i++){
        for (int j = 0; j < N; j++){
            if (i == j){
                continue;
            }
            if (slices[i] > slices[j]){
                continue;
            }
            for (int k = 0; k < N; k++){
                if (k == i || k == j){
                    continue;
                }
                for (int l = 0; l < N; l++){
                    if (slices[k] > slices[l]){
                        continue;
                    }
                    if (l == k || l == i || l == j){
                        continue;
                    }
                    int kat1 = slices[j] - slices[i];
                    int kat2 = slices[k];
                    int gip = slices[l];
                    if (kat1 * kat1 + kat2 * kat2 == gip * gip){
                        int new_area = (slices[i] + slices[j]) / 2 * slices[k];
                        if (new_area > area){
                            area = new_area;
                            indexes.clear();
                            indexes.push_back(i);
                            indexes.push_back(j);
                            indexes.push_back(k);
                            indexes.push_back(l);

                        }
                    }
                }
            }
        }
    }
    sort(indexes.begin(), indexes.end());
    if (area == -1){
        cout << -1;
        return 0;
    }
    for (int i : indexes){
        cout <<i <<" ";
    }
}
