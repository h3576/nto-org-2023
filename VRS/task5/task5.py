import sys

sys.setrecursionlimit(40000)


def do_ways(p, time):
    if time >= min_time[p]:
        return
    min_time[p] = time
    for trip in ways[p]:
        do_ways(trip[0], time + trip[1])


n, a, b = map(int, input().split())
m, k = map(int, input().split())

holes = [[] for i in range(n + 1)]
ways = [[] for i in range(n + 1)]

for i in range(m):
    ai, bi, ti, dt = map(int, input().split())
    holes[ai].append([bi, ti, dt])

for i in range(k):
    ai, bi, ti = map(int, input().split())
    ways[ai].append([bi, ti])

min_time = [10 ** 10 for i in range(n + 1)]

do_ways(a, 0)

# print(min_time)

print(min_time[b])
