N = int(input())

slices = []

for i in range(N):
    slices.append(int(input()))

area = -1
indexes = []

for i in range(N):
    for j in range(N):
        if i == j:
            continue
        if slices[i] > slices[j]:
            continue
        for k in range(N):
            if k == i or k == j:
                continue
            for l in range(N):
                if slices[k] > slices[l]:
                    continue
                if l == k or l == i or l == j:
                    continue
                kat1 = slices[j] - slices[i]
                kat2 = slices[k]
                gip = slices[l]
                if kat1 ** 2 + kat2 ** 2 == gip ** 2:
                    new_area = (slices[i] + slices[j]) / 2 * slices[k]
                    if new_area > area:
                        area = new_area
                        indexes = [i, j, k, l]

indexes.sort()
if area == -1:
    print(-1)
    exit()

for i in indexes:
    print(i, end=" ")
