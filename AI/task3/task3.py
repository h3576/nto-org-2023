import sys

sys.setrecursionlimit(40000)

shortest_route = []

turns = {
    "L": {"D": "MOVE LEFT", "U": "MOVE RIGHT"},
    "R": {"D": "MOVE RIGHT", "U": "MOVE LEFT"},
    "U": {"L": "MOVE LEFT", "R": "MOVE RIGHT"},
    "D": {"R": "MOVE LEFT", "L": "MOVE RIGHT"},
}


def print_answer():
    direction = 'D'

    for el in shortest_route:
        if direction != el:
            print(turns[direction][el])
            direction = el
        print("GO")
    print("FINISH")
    exit()


def go(route, x, y, visited, inter):
    global min_dist_map
    if (x, y) in visited:
        return
    if min_dist_map[y][x] <= inter:
        return
    min_dist_map[y][x] = inter
    inter += 1
    global shortest_route
    visited.append((x, y))
    if data[y][x] == 'F':
        if len(shortest_route) > len(route) or len(shortest_route) == 0:
            shortest_route = route
        return
    r1 = route.copy()
    r2 = route.copy()
    r3 = route.copy()
    r4 = route.copy()

    if data[y][x + 1] != '#':
        r1.append('R')
        go(r1, x + 1, y, visited.copy(), inter)
    if data[y + 1][x] != '#':
        r2.append('D')
        go(r2, x, y + 1, visited.copy(), inter)
    if data[y][x - 1] != '#':
        r3.append('L')
        go(r3, x - 1, y, visited.copy(), inter)
    if data[y - 1][x] != '#':
        r4.append('U')
        go(r4, x, y - 1, visited.copy(), inter)


n, m = map(int, input().split())

min_dist_map = [[9999999 for i in range(m)] for j in range(n)]
data = []
for i in range(n):
    data.append(input())

for i in range(n):
    for j in range(m):
        if data[i][j] == 'S':
            r = []
            v = []
            go(r, j, i, v, 0)
            break

print_answer()
