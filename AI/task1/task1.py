n, k = map(int, input().split())
B = list(map(int, input().split()))
C = list(map(int, input().split()))

q = {}
for i in range(n):
    q[i] = B[i] / C[i]

q = dict(sorted(q.items(), key=lambda item: item[1], reverse=True))

to_use = []
i = 0
for w in q:
    to_use.append(w)
    i += 1
    if i >= k:
        break

to_use = sorted(to_use)

for i in range(k):
    print(to_use[i] + 1, end=" ")