import math

import numpy as np

n, m = map(int, input().split())
matrix = []
elems = set()


for i in range(n):
    matrix.append(list(map(int, input().split())))

for i in range(n):
    for j in range(m):
        if matrix[i][j] not in elems:
            elems.add(matrix[i][j])

matrix = np.array(matrix)

triangles = 0

for i in elems:
    search = np.where(matrix == i)
    for j in range(len(search[0])):
        for o in range(j + 1, len(search[0])):
            for k in range(o + 1, len(search[0])):
                point1 = (search[0][j], search[1][j])
                point2 = (search[0][o], search[1][o])
                point3 = (search[0][k], search[1][k])
                sideA = math.sqrt((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2)
                sideB = math.sqrt((point2[0] - point3[0]) ** 2 + (point2[1] - point3[1]) ** 2)
                sideC = math.sqrt((point3[0] - point1[0]) ** 2 + (point3[1] - point1[1]) ** 2)
                min_side = min(sideA, sideB, sideC)
                max_side = max(sideA, sideB, sideC)
                mid_side = sideA + sideB + sideC - min_side - max_side
                # print(max_side ** 2 - min_side ** 2 - mid_side ** 2)
                if abs(max_side - mid_side - min_side)  < 0.0001:
                    continue
                if max_side ** 2 - min_side ** 2 - mid_side ** 2 > 0.000000001:
                    triangles += 1

print(triangles)