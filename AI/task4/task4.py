n, m, k = map(int, input().split())

f = 1 / (k + 1)

arr = [[0 for i in range(m + 1)] for j in range(n + 1)]

arr[0][0] = 1

for i in range(1, n+1):
    for j in range(0, m + 1):
        h = 0
        while h <= k and h <= j:
            arr[i][j] += arr[i - 1][j - h] * f
            h += 1

print(float('{:.6f}'.format(arr[n][m])))