def find_subarray(a, b):
    counter = 0
    for i in range(len(a) - len(b)):
        if a[i:i + len(b)] == b:
            counter += 1
    if counter > 0:
        return counter ** 2
    return 0


n, m = map(int, input().split())
B = list(map(int, input().split()))

ans = 0

for i in range(m):
    fragment = list(map(int, input().split()))[1:]
    ans += find_subarray(B, fragment)

print(ans)
