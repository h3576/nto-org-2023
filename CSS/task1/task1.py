n, w = map(int, input().split())

fence = list(map(int, input().split()))

min_sum = 99999999999999999999999

for i in range(n):
    part1 = fence[i: min(len(fence), i + w)]
    part2 = fence[: max(0, i + w - len(fence))]
    part = part1 + part2
    min_sum = min(sum(part), min_sum)

print(min_sum)