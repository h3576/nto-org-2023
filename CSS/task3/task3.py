prices = list(map(int, input().split()))

n = int(input())

for i in range(n):
    new_price = prices[-1]
    index = 10 + i
    while index > 0:
        new_price += prices[index % 10]
        index //= 10
    prices.append(new_price)


for i in range(n):
    print(prices[int(input())])
