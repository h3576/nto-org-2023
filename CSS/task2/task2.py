import math

n, k = map(int, input().split())

nums = list(map(int, input().split()))

total = 0

for i in range(n):
    # print("Cycle: " + str(i))
    combos = k ** (i + 1)
    if 0 in nums:
        # print(k ** i)
        combos -= k ** i
    total += combos

print(total)