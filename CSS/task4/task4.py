to_use = [1, 10, 10**2, 10**3, 10**4, 10**5, 10**6, 10**7]

n = int(input())

delta = 0

prices = list(map(int, input().split()))

for old_price in prices:
    new_price = 0
    j = 0
    while new_price < old_price:
        new_price = to_use[j]
        j += 1
    if old_price < new_price // 2:
        new_price = new_price // 10
    delta += new_price - old_price

print(delta)