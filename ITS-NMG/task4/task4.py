real = 0
norm = 0
for i in range(12):
    r, d = map(int, input().split())
    real += r
    norm += r / (d / 100 + 1)

print(real / norm * 100 - 100)