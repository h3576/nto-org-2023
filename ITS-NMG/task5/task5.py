holy_nums = [3, 4, 5, 7, 8]
replacers = {3: 4, 4: 5, 5: 7, 7: 8, 8: 9, 9: 9}
num = input()
num_array = []
for i in range(len(num)):
    num_array.append(int(num[i]))


for i in range(len(num_array) - 1, -1, -1):
    if num_array[i] in holy_nums:
        continue
    for h in holy_nums:
        if h > num_array[i]:
            for j in range(i+1, len(num_array)):
                num_array[j] = 3
            num_array[i] = h
            break
        if h == 8:
            num_array[i] = 9
for i in range(len(num_array) - 1, -1, -1):
    if num_array[i] == 9:
        for j in range(i, len(num_array)):
            num_array[j] = 3
        if i == 0:
            num_array = [3] + num_array
            break
        num_array[i - 1] = replacers[num_array[i - 1]]

for el in num_array:
    print(el, end="")