n1, p1 = map(int, input().split())
n2, p2 = map(int, input().split())
t = int(input())
s = int(input())

print(round(((n1 * p1 + n2 * p2) * t * s) / 100000))
