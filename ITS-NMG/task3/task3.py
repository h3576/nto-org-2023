n, m, k = map(int, input().split())

print(min(max(m - k, 0) + min(k, m) * 2, n))
